To run the assessment, use the docker-compose file in this repo (the extra_hosts entry was required for me, running Ubuntu in Virtual Box).

The spec has been implemented in the following docker image:  [gpitman0xff/st-assessment:v1](https://hub.docker.com/repository/docker/gpitman0xff/st-assessment)

The python file (sub.py) is being executed in the container, but is included in this repo for easy reference.

To query the persistent storage once the container is running, run the following commands:

```
docker exec -it assessment bash
sqlite3 sporttrade.db "SELECT * FROM event;"
sqlite3 sporttrade.db "SELECT * FROM execution;"
```
