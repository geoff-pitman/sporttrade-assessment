import asyncio
import datetime
import sqlite3
from nats.aio.client import Client
from pbgen import event_pb2, execution_pb2

async def run(loop):
	conn = sqlite3.connect('sporttrade.db')
	cursor = conn.cursor()

	client = Client()

	await client.connect('host.docker.internal:4222', loop=loop)

	async def sport_event_handler(msg):
		event = event_pb2.event()
		event.ParseFromString(msg.data)
		cursor.execute(f"INSERT INTO event VALUES({event.sport}, '{event.match_title}', '{event.data_event}', '{datetime.datetime.now()}');")
		conn.commit()

	await client.subscribe('sport_event', 'sq', sport_event_handler)

	async def execution_handler(msg):
		execution = execution_pb2.execution()
		execution.ParseFromString(msg.data)
		cursor.execute(f"INSERT INTO execution VALUES('{execution.symbol}', '{execution.market}', {execution.price}, {execution.quantity}, {execution.executionEpoch}, '{execution.stateSymbol}', '{datetime.datetime.now()}');")
		conn.commit()

	await client.subscribe('execution', 'eq' , execution_handler)

if __name__ == '__main__':
	loop = asyncio.get_event_loop()
	loop.run_until_complete(run(loop))
	loop.run_forever()
